$(function(){
	var arrow_up = "&#x25B2;";
	var arrow_down = "&#x25BC;";
	
	$("div.fold-body").hide();
	$(".fold-head").prepend("<span class='fold-arrow'>" + arrow_down + "</span>");
	$(".fold-arrow").css("margin-right", "5px");
	$(".fold-arrow").css("user-select", "none");
	
	function close_fold(){
		$(this).children('.fold-head').children('.fold-arrow').html(arrow_down);
		$(this).children('.fold-body').slideUp(250);
	}
		
	function open_fold(){
		$(this).children('.fold-head').children('.fold-arrow').html(arrow_up);
		$(this).children('.fold-body').slideDown(250);
	}
	
	$(".fold-head").click(function(e){
		var fold = $(this).parent();
		
		if ( fold.children('.fold-body').is(":visible") ){ // open
			fold.each(close_fold);
		} else { // closed
			$.each(fold.attr('class').split(/\s+/), function(i, c){
				if (c.startsWith("fold-accordion")) {
					$('.fold.' + c).each(close_fold);
				}
			});
			fold.each(open_fold);
		}
	});
});
